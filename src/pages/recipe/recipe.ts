import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, PopoverController, MenuController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


import { PreparationPage } from '../../components/recipes/preparation';
import { RecipeDetailsPage } from '../recipe-details/recipe-details';
import { FiltersPage } from './filters';
import { SettingsPage } from '../settings/settings';

import { Recipe } from '../../providers/recipe';
// import { DataProvider} from '../../providers/data-provider';


/*
  Generated class for the Recipe page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
    selector: 'page-recipe',
    templateUrl: 'recipe.html'
  })
  export class RecipePage {

    myHeaderFn(record, recordIndex, records) {
      if (recordIndex % 20 === 0) {
        return 'Header ' + recordIndex;
      }
      return null;
    }

    dismissed:any = [];
    viewed:any = [];
    prepared:any = [];
    favorites:any = [];

    recipeId: string;

    currentFilter: string = '';

    recipeType: string = "all";


    recipes: Array<any> = [];
    shouldShowSearch: boolean = false;
    allRecepies: Array<any>;

    constructor(public navCtrl: NavController, public storage: Storage, private recipeProvider: Recipe, public modalCtrl: ModalController, public popoverCtrl: PopoverController, public menuCtrl: MenuController) {

      // OPTION A: Use static constructor

      this.navCtrl.viewWillEnter.subscribe(() => {
        
      });

      this.menuCtrl.swipeEnable(true, 'settingsMenu');
      this.menuCtrl.swipeEnable(true, 'userMenu');

      this.menuCtrl.enable(true, 'settingsMenu');
      this.menuCtrl.enable(true, 'userMenu');

      recipeProvider.loadUserCredentials().then((result) => {
          this.loadAllRecipes();
      }, (reason) => {
        console.log(reason);
      });
      this.storage.ready().then(() => {
        this.storage.get('viewed').then((result) => {
          if(result) {
            this.viewed = result;
          }
        });
        this.storage.get('dismissed').then((result) => {
          if(result) {
            this.dismissed = result;
          }
        });
        this.storage.get('prepared').then((result) => {
          if(result) {
            this.prepared = result;
          }
        });
        this.storage.get('favorites').then((result) => {
          if(result) {
            this.favorites = result;
          }
        });
      });
      
    }
    recipeTypeChanged() {
      this.shouldShowSearch = false;
      console.log(this.recipeType);
    }
    toggleSearch() {
      this.shouldShowSearch = !this.shouldShowSearch;
    }
    recipeSelecter(recipe) {
      console.log(recipe);
    }
    doRefresh(refresher) {
      this.recipeProvider.all().then((recipes: Array<any>) => {
        this.allRecepies = recipes;
        this.toggleFilter(this.currentFilter);
        if(refresher) refresher.complete();
      },reason => {
        if(refresher) refresher.complete();
      })
    }
    loadAllRecipes() {
      this.recipeProvider.all().then((recipes: Array<any>) => {
        this.allRecepies = recipes;
        this.toggleFilter(this.currentFilter);
      }, (reason) => {
        console.log(reason);
      })
    }
    showDetails(item) {
      // this.navCtrl.push(RecipeDetailsPage, {recipe: item});
      this.viewed.push(item.id);
      this.storage.set('viewed',this.viewed);
      this.navCtrl.push(RecipeDetailsPage, {recipe: item, shouldStart: false});

      // recipeModal.present();
    }
    showRandom() {
      let rand = this.allRecepies[Math.floor(Math.random() * this.allRecepies.length)];
      this.showDetails(rand);
    }
    prepare(item) {
      let prepareModal = this.modalCtrl.create(PreparationPage, {recipe: item, shouldStart: true});
      prepareModal.onDidDismiss((result) => {
        console.log(result);
      });
      prepareModal.present();
    }


    searchItems(ev) {
      let val = ev.target.value;
      if (val && val.trim() != '') {
        this.recipes = this.allRecepies.filter((item) => {
          return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
      } else {
        this.recipes = this.allRecepies
      }
    }
    showSettings(myEvent) {
      let settingsModal = this.modalCtrl.create(SettingsPage, { userId: 8675309 });
      settingsModal.onDidDismiss((settings) => {
        console.log(settings);
      });
      settingsModal.present();
    }
    showFilter(myEvent) {
      let popover = this.popoverCtrl.create(FiltersPage);
      popover.onDidDismiss(data => {
          console.log("popover dismissed");
          console.log("Selected Item is " + data);
      });
      popover.present({
        ev: myEvent
      });
    }
    toggleFilter($filterName) {
      console.log($filterName);
      if(this.currentFilter == $filterName) {
        this.currentFilter = '';
      } else {
        this.currentFilter = $filterName;
      }
      this.recipes = [];
      switch (this.currentFilter) {
        case 'favorites':
          this.storage.get('favorites').then((result) => {
            if(!result) result = [];
            this.favorites = result;
            this.recipes = [];
            this.allRecepies.forEach(element => {
              if(this.favorites.includes(element.id))
                this.recipes.push(element);
            });
          })
          
          break;
        case 'viewed':
        case 'top-views':
          this.storage.get('viewed').then((result) => {
            if(!result) result = [];
            this.viewed = result;
            this.recipes = [];
            this.allRecepies.forEach(element => {
              if(this.viewed.includes(element.id))
                this.recipes.push(element);
            });
          });
          break;
        case 'prepared':
          this.storage.get('prepared').then((result) => {
            if(!result) result = [];
            this.prepared = result;
            this.recipes = [];
            this.allRecepies.forEach(element => {
              if(this.prepared.includes(element.id))
                this.recipes.push(element);
            });
          });
          break;
        case 'dismissed':
          this.storage.get('dismissed').then((result) => {
            if(!result) result = [];
            this.dismissed = result;
            this.recipes = [];
            this.allRecepies.forEach(element => {
              if(this.dismissed.includes(element.id))
                this.recipes.push(element);
            });
          });
          break;
        default:
          this.storage.get('dismissed').then((result) => {
            if(!result) result = [];
            this.dismissed = result;

            this.allRecepies.forEach(element => {
              if(!this.dismissed.includes(element.id))
                this.recipes.push(element);
            });
          });
          break;
      }
    }


  }


