import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';


import { DataProvider} from '../../providers/data-provider';


@Component({
  selector: 'filters',
  templateUrl: 'filters.html'
})
export class FiltersPage {

  filters: any;
  countries: any;

  constructor(public viewCtrl: ViewController, data: DataProvider) {
    data.categories().subscribe(
      categories => {
        let filters = new Array();
        for (let c of categories) {
          filters.push({
            title: c,
            checked: false
          })
        }
        this.filters = filters;
      }
      )
    data.countries().subscribe(
      countries => {
        // let countries = new Array();
        // for (let c of categories) {
        //   countries.push({
        //     title: c,
        //     checked: false
        //   })
        // }
        this.countries = countries;
      }
      )
  }
  // selectCategory(category) {
  //   this.currentCategories.push(category);
  //   this.viewCtrl.dismiss(category);
  // }
  close() {
    this.viewCtrl.dismiss();
  }

}
