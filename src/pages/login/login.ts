import { InAppBrowser } from '@ionic-native/in-app-browser';

import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { Auth } from '../../providers/auth';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  loader:any;
  

  isNewUser = true;

  showForm = 'login';

  messages = {
    'invalid_credentials': 'Usuario y contraseña inválidos.',
    'user already registered': 'El correo electrónico ingresado ya se encuentra registrado.'
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, private auth: Auth, private toastCtrl: ToastController, public menuCtrl: MenuController,public alertCtrl: AlertController, private iab: InAppBrowser, loadingCtrl: LoadingController) {

      this.menuCtrl.swipeEnable(false, 'settingsMenu');
      this.menuCtrl.swipeEnable(false, 'userMenu');

      this.menuCtrl.enable(false, 'settingsMenu');
      this.menuCtrl.swipeEnable(false, 'userMenu');

      this.loader = loadingCtrl.create({
        content: 'Espera...'
      });

  }

  toggleForm() {
    if(this.showForm == 'registration') {
      this.showForm = 'login';
    } else {
      this.showForm = 'registration';
    }
  }
  showError(error) {
    if(error.hasOwnProperty('error'))
      error = error.error;
    let message = this.messages[error] || 'Error de autenticación. Revisa tus credenciales e intentalo de nuevo.';
    let alert = this.alertCtrl.create({
         title: 'Error!',
         subTitle: message,
         buttons: ['OK']
       });
       alert.present();
  }

  ionViewDidLoad() {
  }

  fbLogin() {
    this.auth.facebookLogin().then((isLoggedIn) => {
      let toast = this.toastCtrl.create({
        message: 'User is logged in',
        duration: 3000
      });
      toast.present();
    }, (reason) => {
      let toast = this.toastCtrl.create({
        message: reason,
        duration: 3000
      });
      toast.present();
    });
  }
  showPage(url) {

    let browser = this.iab.create(url, '_blank', {
      hidden: 'yes',
      hardwareback: 'yes',
      toolbar: 'yes',
      closebuttoncaption: 'Cerrar',
      location: 'yes',
      toolbarposition: 'top'
    });
    browser.on('loadstart').do(result => { 
      this.hideLoading(); 
    });
    browser.on('loadstop').do(result => { 
      // Show error message
    });
    browser.on('loaderror').do(result => { 
      this.hideLoading(); 
      // Show error message
    });
    browser.show();
  }
  showLoading(msg: string = 'Espera...') {
    this.loader.setContent(msg);
    this.loader.present();
  }
  hideLoading() {
    this.loader.dismiss();
  }
}
