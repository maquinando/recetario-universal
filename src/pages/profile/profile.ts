import { Component, EventEmitter } from '@angular/core';
import { NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { GlobalValidator } from "../../components/GlobalValidator";
import { Camera, CameraOptions } from '@ionic-native/camera';

import { Profile } from '../../providers/profile';
/*
  Generated class for the Profile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  profileForm: FormGroup;
  user: any;
  onSuccess: any = new EventEmitter();
  onError: any = new EventEmitter();
  startTime: any;

  isLoading: boolean = false;

  private options: CameraOptions = {
    quality: 80,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE, 
    targetWidth: 256,
    targetHeight: 256
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, formBuilder: FormBuilder, public profileProvider: Profile, public toastCtrl: ToastController, private camera: Camera, private alertCtrl: AlertController) {
    this.user = navParams.get('user');
    this.profileForm = formBuilder.group({
      email: [this.user.email, Validators.compose([Validators.required, Validators.minLength(5), GlobalValidator.mailFormat])],
      firstname: [this.user.profile.firstname, Validators.compose([Validators.required, Validators.minLength(5)])],
      lastname: [this.user.profile.lastname, Validators.compose([Validators.required, Validators.minLength(5)])],
      birthday: [this.user.profile.birthday],
      gender: [this.user.profile.gender, Validators.compose([Validators.required])],
      country: [this.user.profile.country, Validators.compose([Validators.required])],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }
  close() {
    this.navCtrl.pop();
  }
  update(data) {
    this.profileProvider.update(data)
    .then((result) => {

    }, (reason) => {
      console.error(reason);
    });
  }
  getAvatar() {
    this.camera.getPicture(this.options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.user.profile.avatar = 'data:image/jpeg;base64,' + imageData;
      }, (err) => {
      // Handle error
       let alert = this.alertCtrl.create({
          title: 'Error!',
          subTitle: err,
          buttons: ['OK']
        });
        alert.present();
      });
  }

}
