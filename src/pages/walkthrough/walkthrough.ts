import { TranslateService } from '@ngx-translate/core';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, MenuController } from 'ionic-angular';
import { Onboarding } from '../../components/onboarding/onboarding';
import { LoginPage } from '../login/login';


/*
  Generated class for the Walkthrough page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
    selector: 'page-walkthrough',
    templateUrl: 'walkthrough.html'
  })
  export class WalkthroughPage {


    @ViewChild(Onboarding) onboarding: Onboarding;
    currentIndex: number = 0;

    walkthrough:any = [];

    constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, private translate: TranslateService) {
      this.menuCtrl.swipeEnable(false, 'settingsMenu');
      this.menuCtrl.swipeEnable(false, 'userMenu');
      
      this.menuCtrl.enable(false, 'settingsMenu');
      this.menuCtrl.enable(false, 'userMenu');
      this.walkthrough = [
        {
          title: 'WELCOME',
          subtitle: "Recetario Universal <br>¡Conozcámonos!",
          description: "Regístra tus datos, crea un usuario y estarás listo para comenzar.",
          image: "assets/img/onboarding/1.png",
          icon: "log-in",
        },
        {
          title: "¿Cuál es tu receta preferida?",
          subtitle: "Elíge las recetas que quieres conocer",
          description: "¡Te ayudaremos a prepararlas!",
          image: "assets/img/onboarding/2.png",
          icon: "settings",
        },
        {
          title: "¿Se te antoja algo?",
          subtitle: "Selecciona una receta",
          description: "Elige o busca una receta que quieras preparar.",
          image: "assets/img/onboarding/3.png",
          icon: "search",
        },
        {
          title: "¿Quieres preparala?",
          subtitle: "Tócala y verás paso a paso el proceso",
          description: "o deslisa a la derecha para añadirla a tus favoritas.",
          image: "assets/img/onboarding/4.png",
          icon: "hand",
        },
        {
          title: "Timer Universal<br>¡Tu acompañante al cocinar!",
          subtitle: "No necesitarás ser un experto en la cocina",
          description: "controla el tiempo de tus preparaciones y hazlo paso a paso.",
          image: "assets/img/onboarding/5.png",
          icon: "alarm",
        },
        {
          title: "¡Muéstrale a todos lo que preparaste!",
          subtitle: "¡Huele y sabe delicioso!<br>Merece ser compartito,",
          description: "así ganarás puntos que podrás redimir por beneficios.",
          image: "assets/img/onboarding/6.png",
          icon: "share",
        },
        {
          title: "¡Listo para empezar!",
          subtitle: "¡Presiona el botón!",
          description: "Ahora solo debes tocar el botón Comenzar.",
          image: "assets/img/onboarding/7.png",
          icon: "checkmark",
        }
        ];
    }
    ionViewDidLoad() {

    }
    skip() {
      this.onboarding.skip();
    }
    start() {
      this.navCtrl.setRoot(LoginPage);
    }
    isLastIndex() {
      return this.currentIndex >= this.walkthrough.length - 1;
    }
    slideChanged(index) {
      this.currentIndex = index;
    }

  }
