import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DemoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-demo',
  templateUrl: 'demo.html',
})
export class DemoPage {

  name: string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    let _name = navParams.get('name');
    if(_name) {
      this.name = _name;
    } else {
      this.name = 'Daniel';
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DemoPage');
  }

}
