import { RecipeDetails } from './../../components/recipes/recipe-details';
import { Recipe } from './../../providers/recipe';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


/*
  Generated class for the RecipeDetails page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-recipe-details',
  templateUrl: 'recipe-details.html'
})
export class RecipeDetailsPage {


  @ViewChild(RecipeDetails) details: RecipeDetails;

  recipe: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public storage: Storage, private alertCtrl: AlertController, private recipeProvider: Recipe) {
    let recipe = navParams.get('recipe');
    if(!recipe) {
      let recipeId = navParams.get('recipeId');
      recipeProvider.getRecipe(recipeId).then(
        result => {
          this.recipe = result;
        },
        reason => {
          //alert showing error. Return to home.
          console.log(reason);
        });
    } else {
      this.recipe = recipe;
      this.doRefresh(null);
    }
  }

  close() {
    this.viewCtrl.dismiss();
  }
  onDismiss() {
    this.storage.get('dismissed').then((result) => {
      if(!result) 
        result = [];
      result.push(this.recipe.id);
      this.storage.set('dismissed', result);
    });
    this.close();
  }

  onPrepare() {
     
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecipeDetailsPage');
  }

  doRefresh(refresher) {
    this.recipeProvider.getRecipe(this.recipe.id).then(
      result => {
        if(refresher) refresher.complete();
        this.recipe = result;
        this.details.setRecipe(result);
      },
      reason => {
        if(refresher) refresher.complete();
        console.log(reason);
      }
    );
  }



}
