import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings'

/**
 * Generated class for the SettingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  settings: any = {};
  constructor(public viewCtrl: ViewController, public navParams: NavParams, public settingsProvider: SettingsProvider) {
    settingsProvider.initializePreferences().then((settings) => {
      this.settings = settings;
    }, (reason) => {
      console.log(reason);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingsPage');
  }

  dismiss() {
    this.settingsProvider.updateSettings(this.settings);
    this.viewCtrl.dismiss(this.settings);
  }
}
