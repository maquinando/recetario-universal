import { Events } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

import { ENV } from '../config/environment-dev';
import 'rxjs/add/operator/map';




/*
  Generated class for the Profile provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Profile {

  authToken:string;
  prepared:Array<any>;
  favorites:Array<any>;

    
  


  constructor(public http: Http, public storage: Storage, private events: Events) {
    console.log('Hello Profile Provider');
    storage.ready().then(() => {
      storage.get('favorites').then(result => this.favorites = result);
      storage.get('prepared').then(result => this.prepared = result);
    });
  }

  getProfile() {
    return new Promise((resolve, reject) =>  {
      this.storage.get('AuthToken').then((result) => {
        this.authToken = result;
        var headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.authToken);
        this.http.get(ENV.API_URL + 'user/profile', {headers: headers})
          .subscribe(
            data => {
              console.log(data);
              let user = data.json().user;
              if(!user.profile) user.profile = {};
              user.prepared = this.prepared;
              user.favorites = this.favorites;
              resolve(user);
            },
            err => {
              console.log(err);
              let e = err.json();
              if(e.error == "token_not_provided") {
                this.events.publish('user:logout');
              }
              reject(e);
            }
          );
        });
      });
  }
  update(data) {
    return new Promise((resolve, reject) =>  {
      this.storage.get('AuthToken').then((result) => {
        this.authToken = result;
        var headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.authToken);
        this.http.post(ENV.API_URL + 'user/profile', data, {headers: headers})
          .subscribe(
            result => {
              if(result.ok) {
                resolve(result.json());
              }
            },
            err => {
              console.log(err)
              reject(err);
            }
          );
        });
      });
  }


}
