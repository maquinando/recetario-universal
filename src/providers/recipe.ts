import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Network } from 'ionic-native';
import { Storage } from '@ionic/storage';
import { ENV } from '../config/environment-dev';
import 'rxjs/add/operator/map';

/*
  Generated class for the Recipe provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Recipe {

  AuthToken: String;

  constructor(public http: Http, public storage: Storage) {
    console.log('Hello Recipe Provider');
  }

  storeRecipe(recipe) {

  }
  parseRecipes(recipes: Array<any>) : any {
    let result = [];
    for (let recipe of recipes) {
      result.push(
        {
          id: parseInt(recipe['id']),
          img: 'http://apps.maquinando.co/timerapp/admin/filemanager/connectors?mode=preview&path=/js/filemanager/userfiles/' + recipe['image']['image'],
          preview: 'http://apps.maquinando.co/timerapp/admin/filemanager/connectors?mode=preview&thumbnail=true&path=/js/filemanager/userfiles/' + recipe['image']['image'],
          preparationTime: recipe['preparation_time'],
          slug: recipe['slug'],
          updatedAt: recipe['updated_at'],
          title: recipe['name'],
          excerpt: recipe['content'].replace(/<\/?[^>]+(>|$)/g, "").substring(0,60) + ' ...',
          cals: parseInt(recipe['calories']),
          portions: parseInt(recipe['portions']),
          content: recipe['content']
        });
    }
    return result;
  }
  public all() {
      return new Promise((resolve, reject) => {
        var headers = new Headers();
        // headers.append('Authorization', 'Bearer ' + this.AuthToken);

          this.http.get(ENV.API_URL + 'recipes', {headers: headers}).subscribe((data) => {
            // if(data.json().success){
              resolve( this.parseRecipes(data.json().data) );
            // }
            // else
              // reject(false);
          }, (reason) => {
            reject(reason)
          });
      });
  }
  public setAuthToken(token) {
      this.AuthToken = token;
  }
  public getRecipe(recipeId) {
    return new Promise((resolve, reject) => {

      this.http.get(ENV.API_URL + 'recipes/' + recipeId).subscribe(
        data => {
          if(data.ok){
            let recipe = data.json();
            recipe['img'] = 'http://apps.maquinando.co/timerapp/admin/filemanager/connectors?mode=preview&path=/js/filemanager/userfiles/' + recipe['image']['image'];
            recipe['preview'] = 'http://apps.maquinando.co/timerapp/admin/filemanager/connectors?mode=preview&thumbnail=true&path=/js/filemanager/userfiles/' + recipe['image']['image'];
            recipe['excerpt'] = recipe['content'].replace(/<\/?[^>]+(>|$)/g, "").substring(0,60) + ' ...';
            recipe['cals'] = parseInt(recipe['calories']);
            recipe['preparationTime'] = recipe['preparation_time'];
            
            resolve(recipe);
          }
          else {
            reject('Error al llamar el servicio');
          }      
        }, 
        reason => {
          reject(reason);
        });
    });

  }
  useCredentials(token: String) {
    this.AuthToken = token;
  }
  public loadUserCredentials() : Promise<any>{
    return new Promise((resolve, reject) => {
        if(this.AuthToken) {
          resolve(this.AuthToken);
          return;
        }
        this.storage.get('AuthToken').then((token) => {
          if(token) {
            this.useCredentials(token);
            resolve(token);
          } else {
            reject('User is not logged in');
          }
        }, (reason) => {
          reject(reason);
        });
      });
  }
}
