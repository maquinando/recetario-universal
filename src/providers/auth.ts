import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Facebook, Network } from 'ionic-native';
import { ENV } from '../config/environment-dev';
import { JwtHelper } from 'angular2-jwt';
import { Platform, Events } from 'ionic-angular';


import 'rxjs/add/operator/map';

  /**
  * Generated class for the Auth provider.
  * See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  * For more info on providers and Angular 2 DI.
  **/
  @Injectable()
  export class Auth {
    window: any;
    isLoggedIn: Boolean;
    AuthToken: string;
    isConnected: Boolean;
    isOnWifi: Boolean;
    modalCtrl;

    jwtHelper: JwtHelper = new JwtHelper();


    connectSubscription: any;
    disconnectSubscription: any;
    platforms: any;

    constructor(public http: Http, public storage: Storage, public plt: Platform, private events: Events) {

      this.AuthToken = null;
      this.isLoggedIn = false;

      this.connectSubscription = Network.onConnect().subscribe(() => {
        this.isConnected = true;
        setTimeout(() => {
          if (Network.type === 'wifi') {
            this.isOnWifi = true;
          }
        }, 1000);
      });
      this.disconnectSubscription = Network.onDisconnect().subscribe(() => {
        this.isConnected = false;
        this.isOnWifi = false;
      });
      storage.ready().then(() => {
        // set a key/value
        this.loadUserCredentials().then(
          result => {console.log(result)},
          err => {console.error(err)}
          );
      });

      this.platforms = this.plt.platforms();


    }
    storeUserCredentials(token) {
      this.storage.set('AuthToken', token).then((result) => {
          console.log(result);
      }, (reason) => {
        console.error(reason);
      });
      this.useCredentials(token);
    }
    destroyUserCredentials() {
      this.events.publish('user:logout');
      this.isLoggedIn = false;
      this.AuthToken = null;
      this.storage.remove('AuthToken');
    }

    useCredentials(token: string) {
      this.events.publish('user:login');
      this.isLoggedIn = true;
      this.AuthToken = token;
    }
    loadUserCredentials() : Promise<any>{
      return new Promise((resolve, reject) => {
          if(this.AuthToken) {
            let isExpired = this.jwtHelper.isTokenExpired(this.AuthToken);
            if(isExpired) {
              this.destroyUserCredentials();
              reject('User session is expired.');
            } else {
              this.events.publish('user:login');
              resolve(this.AuthToken);
            } 
            return;
          }
          this.storage.get('AuthToken').then((token) => {
            if(token) {
              let isExpired = this.jwtHelper.isTokenExpired(token);
              if(isExpired) {
                this.destroyUserCredentials();
                reject('User session is expired.');
              } 
              this.useCredentials(token);
              resolve(token);
            } else {
              this.events.publish('user:logout');
              reject('User is not logged in');
            }
          }, (reason) => {
            reject(reason);
          });
        });
    }


    authenticate(user) : Promise<any> {
      var payload = "email=" + (user.email || user.user) + "&password=" + user.password;
      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');


      return new Promise((resolve, reject) => {
        this.http.post(ENV.API_URL + 'login', payload, {headers: headers}).subscribe(data => {
          if(data.ok) {
            this.storeUserCredentials(data.json().token);
          }
          resolve(data.ok);
          // resolve(data.json());
        }, (reason) => {
          reject(reason.json());
        });
      });
    }

    public getUserData(): Promise<any> {
      return new Promise((resolve, reject) => {
        if( !this.isLoggedIn ) {
          reject('User is not logged in');
          return;
        }
        this.loadUserCredentials().then((token) => {
          resolve( this.jwtHelper.decodeToken(token));
        }, (reason) => {
          reject(reason);
        });
      });
    }

    public facebookLogin(): Promise<any> {
      return new Promise((resolve, reject) => {

        let permissions = ["public_profile"];
        Facebook.login(permissions).then((response) => {
            var payload = "token=" + response.authResponse.accessToken + '&userid=' + response.authResponse.userID;
            var headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            this.http.post(ENV.API_URL + 'social/login/facebook', payload, {headers: headers}).subscribe(data => {
              if(data.ok){
                this.storeUserCredentials(data.json().token);
              }
              resolve(data.ok);
              // resolve(data.json());
            }, (reason) => {
              reject( reason.json() );
            });
        }, (reason) => {
          reject(reason);
        });
      });
    }
    public login(userData) : Promise<any> {

      return new Promise((resolve, reject) => {
        if(userData) {
          this.authenticate(userData).then((response) => {
            resolve(response);
          },(reason) => {
            reject(reason);
          });
        } else {
          this.loadUserCredentials().then(() => {
            resolve(this.isLoggedIn);
          }, () => {
            resolve(this.isLoggedIn);
          });
        }
      });
    }
    public recover(email) : Promise<any> {
      var payload = "email=" + email;

      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');

      return new Promise((resolve, reject) => {
        this.http.post(ENV.API_URL + 'recover', payload, {headers: headers}).subscribe(data => {
          if(data.ok) {
            this.storeUserCredentials(data.json().token);
          }
          resolve(data.ok);
          // resolve(data.json());
        }, (reason) => {
          reject( reason.json() );
        });
      });
    }
    private encodeData(data) : String {
      let i = 0;
      let payload = '';
      for(let k in data) {
        if(i>0) payload += '&';
        payload += k + '=' + encodeURIComponent( data[k] );
        i++;
      }
      return payload;
    }
    public register(userData) : Promise<any> {
      var data = {
        name: userData.firstname + ' ' + userData.lastName,
        email: userData.email,
        password: userData.password,
        firstname: userData.firstname,
        lastname: userData.lastname,
        birthday: userData.birthday,
        gender: userData.gender,
        country: userData.country
      }
      var payload = this.encodeData(data);

      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');


      return new Promise((resolve, reject) => {
        this.http.post(ENV.API_URL + 'register', payload, {headers: headers}).subscribe(data => {
          let result = data.json();
          if(data.ok && result.token) {
            this.storeUserCredentials(result.token);
            resolve(data.ok);
          } else {
            reject('Error de autenticación.');
          }
        }, (reason) => {
          reject( reason.json() );
        });
      });

    }
    public logout() : Promise<any> {
      return new Promise((resolve, reject) => {
        this.destroyUserCredentials();
        resolve(true);
      });
    }

    ngOnDestroy() {
      this.connectSubscription.unsubscribe();
      this.disconnectSubscription.unsubscribe();
    }
    registerNotificationsToken(result) {
      let token = result.token;
      return new Promise((resolve, reject) => {
        var _platforms = JSON.stringify(this.platforms);
        var payload = "devicetoken=" + token + "&platforms=" + _platforms;

        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        if(this.AuthToken) {
          headers.append('Authorization', 'Bearer ' + this.AuthToken);
        }

        this.http.post(ENV.API_URL + 'notifications', payload, {headers: headers}).subscribe(data => {
          if(data.ok){
            this.storeUserCredentials(data.json().token);
          }
          resolve(data.json());
        }, (reason) => {
          reject( reason.json() );
        });
      });
    }

  }
