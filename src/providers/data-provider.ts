import { Injectable, OnInit, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';

import { ENV } from '../config/environment-dev';

import { Platform, LoadingController, ToastController } from 'ionic-angular';
import { Network } from 'ionic-native';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

/**
*** Generated class for the DataProvider provider.
***
*** See https://angular.io/docs/ts/latest/guide/dependency-injection.html
*** for more info on providers and Angular 2 DI.
***/
@Injectable()
export class DataProvider implements OnDestroy {

  isLoggedIn: Boolean;
  AuthToken: String;
  isConnected: Boolean;
  isOnWifi: Boolean;

  connectSubscription: any;
  disconnectSubscription: any;

  platforms: any;

  constructor(public http: Http, public storage: Storage) {
    console.log('Hello DataProvider Provider');
    storage.ready().then(() => {
      this.loadUserCredentials().then(
        result => {console.log(result)},
        err => {console.error(err)}
        );

    })
    this.connectSubscription = Network.onConnect().subscribe(() => {
      this.isConnected = true;
      setTimeout(() => {
        if (Network.type === 'wifi') {
          this.isOnWifi = true;
        }
      }, 1000);
    });
    this.disconnectSubscription = Network.onConnect().subscribe(() => {
      this.isConnected = false;
      this.isOnWifi = false;
    });
  }


  storeUserCredentials(token) {
    this.storage.set('AuthToken', token).then((result) => {
      console.log(result);
    }, (reason) => {
      console.error(reason);
    });
    this.useCredentials(token);
  }
  destroyUserCredentials() {
    this.isLoggedIn = false;
    this.AuthToken = null;
    this.storage.remove('AuthToken');
  }
  useCredentials(token: String) {
    this.isLoggedIn = true;
    this.AuthToken = token;
  }
  loadUserCredentials() : Promise<any>{
    return new Promise((resolve, reject) => {
      if(this.AuthToken) {
        resolve(this.AuthToken);
        return;
      }
      this.storage.get('AuthToken').then((token) => {
        if(token) {
          this.useCredentials(token);
          resolve(token);
        } else {
          reject('User is not logged in');
        }
      }, (reason) => {
        reject(reason);
      });
    });
  }
  categories() {
    return Observable.create(observer => {
      observer.next(['Desayuno', 'Almuerzo', 'Cena', 'Postre', 'Para sorprender'])
    })
  }
  countries() {
    return Observable.create(observer => {
      observer.next([{
        title: 'Colombia',
        flag: ''
      }, {
        title: 'Honduras',
        flag: ''
      }])
    })
  }

  ngOnDestroy() {
    this.connectSubscription.unsubscribe();
    this.disconnectSubscription.unsubscribe();
  }
}
