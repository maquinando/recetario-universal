import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/map';

/*
  Generated class for the Settings provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
    */
  @Injectable()
  export class SettingsProvider {


    private defaults:any = {
      parameters: [{
          name: 'Tiempo máximo de preparación',
          value: 35,
          range: [5, 120],
          units: 'min',
          isEnabled: true
        },
        {
          name: 'Calorías Máximas',
          value: 30,
          range: [2, 370],
          units: 'KCal',
          isEnabled: false
        }],
      categories: [
        {
          name: 'Colombiana',
          isEnabled: true
        },
        {
          name: 'Italiana',
          isEnabled: true
        },
        {
          name: 'Francesa',
          isEnabled: true
        }
      ]
    };

    static get PREF_INITIALIZED() { return 'preferencesInitialized';}
    static get PREF_DISCOVERABLE() { return 'pref_discoverable';}
    static get PREF_NOTIFY_MESSAGES() { return 'pref_notification_messages';}
    static get PREF_NOTIFY_INVITES() { return 'pref_notification_invites';}
    static get ALL_PREFERENCES() { return 'pref_all_settings';}


    _preferences: any;

    constructor(public http: Http, public storage: Storage) {
      console.log('Hello Settings Provider');
      this._preferences = {};
    }

    initializePreferences(){
      return new Promise((resolve, reject) => {
        
        console.log('initializePreferences');
        this.storage.get(SettingsProvider.PREF_INITIALIZED).then((result) => {
          if(result == true) {
            this.storage.get(SettingsProvider.ALL_PREFERENCES).then((result) => {
              resolve(result);
            }, (reason) => {
              reject(reason);
            });
          } else {
            this.storage.set(SettingsProvider.ALL_PREFERENCES, this.defaults);
            resolve(this.defaults);
          }
        }, (reason) => {
          console.log('Error cargando la configuración de la app. ' + reason);
          reject(reason);
        })
      });
      
      // this.storage.get(SettingsProvider.PREF_INITIALIZED).then((result) => {
      //   if(result == null || result == false){
      //     console.log('initializePreferences with default values');
      //     this.storage.set(SettingsProvider.PREF_INITIALIZED, true);
      //     this.storage.set(SettingsProvider.PREF_DISCOVERABLE, true);
      //     this.storage.set(SettingsProvider.PREF_NOTIFY_MESSAGES, true);
      //     this.storage.set(SettingsProvider.PREF_NOTIFY_INVITES, true);

      //     //initialize in memory preferences
      //     this._preferences[SettingsProvider.PREF_DISCOVERABLE] = true;
      //     this._preferences[SettingsProvider.PREF_NOTIFY_MESSAGES] = true;
      //     this._preferences[SettingsProvider.PREF_NOTIFY_INVITES] = true;
      //   } else {
      //     console.log('preferences obtained from storage');
      //     let prefs =
      //     [
      //     SettingsProvider.PREF_DISCOVERABLE,
      //     SettingsProvider.PREF_NOTIFY_MESSAGES,
      //     SettingsProvider.PREF_NOTIFY_INVITES
      //     ];

      //     let thisRef = this;
      //     this._getAllPreferences(prefs).then(function(results){
      //       //initialize in memory preferences
      //       for(let i = 0; i < prefs.length; i++){
      //         thisRef._preferences[prefs[i]] = results[i];
      //       }
      //     }, function (err) {
      //       // If any of the preferences fail to read, err is the first error
      //       console.log(err);
      //     });
      //   }
      // });
    }

    updateSettings(preferences) {
      this.storage.set(SettingsProvider.ALL_PREFERENCES, this.defaults);
    }


    getSetting(key) {
      return this._preferences[key];
    }
    setSetting(key, value) {
      this._preferences[key] = value;
      this.storage.set(key, value);
    }
    _getAllPreferences(prefs){
      return Promise.all(prefs.map((key) => {
        return this.storage.get(key);
      }));
    }
    _getPreference(key){
      return this.storage.get(key);
    }

  }
