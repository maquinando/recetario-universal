import { DemoPageModule } from './../pages/demo/demo.module';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Http, HttpModule, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { IonicImageLoader } from 'ionic-image-loader';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { SettingsPageModule } from '../pages/settings/settings.module';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { RecipePage } from '../pages/recipe/recipe';
import { FiltersPage } from '../pages/recipe/filters';
import { RecipeDetailsPage } from '../pages/recipe-details/recipe-details';

import { OnboardingModule } from '../components/onboarding/onboarding.module';
import { RecipePreviewModule } from '../components/recipes/recipe.module';
import { PreparationPage } from '../components/recipes/preparation';

import { ProfileComponentModule } from '../components/profile/profile.module';
import { RecetappAuthModule } from './../components/authentication/authentication.module';

import { Auth } from '../providers/auth';
import { Profile } from '../providers/profile';
import { DataProvider } from '../providers/data-provider';
import { Recipe } from '../providers/recipe';


import { IonicStorageModule, Storage } from '@ionic/storage';
import { CloudSettings, CloudModule, Push, PushToken } from '@ionic/cloud-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SocialSharing } from '@ionic-native/social-sharing';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Camera } from '@ionic-native/camera';
import { Device } from '@ionic-native/device';


const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '547b1475'
  },
  'push': {
    'sender_id': '438752399077',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#343434'
      }
    }
  }
};

export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig(), http, options);
}
export function getAuthHttp(http: Http, option: RequestOptions, storage) {
  return new AuthHttp(new AuthConfig({
    noJwtError: true,
    globalHeaders: [{'Accept': 'application/json'}],
    tokenGetter: (() => storage.get('AuthToken')),
  }), http);
}

export function createTranslateLoader(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    WalkthroughPage,
    LoginPage,
    RecipePage,
    FiltersPage,
    RecipeDetailsPage,
    ProfilePage,
    PreparationPage,
  ],
  imports: [
    OnboardingModule,
    RecipePreviewModule,
    ProfileComponentModule,
    RecetappAuthModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: "Volver",
      modalEnter: 'modal-slide-in',
      modalLeave: 'modal-slide-out',
      monthNames: ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio','agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre' ],
      monthShortNames: ['ene', 'feb', 'mar','abr', 'may', 'jun', 'jul','ago', 'sept', 'oct', 'nov', 'dic' ],
      dayNames: ['domingo', 'lunes', 'marte', 'miercoles', 'jueves', 'viernes', 'sabado' ],
      dayShortNames: ['dom', 'lun', 'mar', 'mie', 'jue', 'vie', 'sab'],
    }),
    BrowserModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [Http]
      }
    }),
    CloudModule.forRoot(cloudSettings),
    IonicStorageModule.forRoot({
      name: '__timerappdb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    IonicImageLoader.forRoot(),
    SettingsPageModule,
    DemoPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    WalkthroughPage,
    LoginPage,
    RecipePage,
    FiltersPage,
    RecipeDetailsPage,
    ProfilePage,
    PreparationPage
  ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: IonicErrorHandler
    },
    {
      provide: AuthHttp,
      useFactory: getAuthHttp,
      deps: [Http, RequestOptions, Storage]
    },
    Auth,
    Profile,
    DataProvider,
    Recipe,
    StatusBar,
    SocialSharing, 
    Camera,
    InAppBrowser,
    Device]
})
export class AppModule { }
