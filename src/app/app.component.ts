import { Storage } from '@ionic/storage';
import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Platform, LoadingController, Nav, ToastController, Events } from 'ionic-angular';
import { Splashscreen, Deeplinks } from 'ionic-native';

import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { RecipePage } from '../pages/recipe/recipe';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { Auth } from '../providers/auth';
import { ProfilePreviewComponent } from '../components/profile/profile-preview';

import { Push, PushToken } from '@ionic/cloud-angular';
import { StatusBar  } from '@ionic-native/status-bar';
import { Device } from '@ionic-native/device';

import { TranslateService } from '@ngx-translate/core';


@Component({
  templateUrl: 'app.html'
})

export class MyApp implements AfterViewInit {
  // private FB_APP_ID: number = 159754064538400

  rootPage: any;
  isLoggedIn: boolean = false;
  loader = this.loadingCtrl.create({
    content: 'Espera...'
  });

  @ViewChild(Nav) navChild:Nav;
  @ViewChild(ProfilePreviewComponent) profilePreview:ProfilePreviewComponent;

  constructor(private platform: Platform, statusBar: StatusBar, public auth: Auth, public loadingCtrl: LoadingController, private push: Push, private toastCtrl: ToastController, translate: TranslateService, events: Events, public device: Device, private storage: Storage) {

    translate.setDefaultLang('es');

    platform.ready().then(() => {
      
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.backgroundColorByHexString('#004382');
      statusBar.styleLightContent();

      try {
        this.push.register().then((t: PushToken) => {
          this.auth.registerNotificationsToken(t).then((result) => {
            console.log(result);
          }, (reason) => {
            console.error(reason);
          });
          return this.push.saveToken(t);
        }).then((t: PushToken) => {
          console.log('Token saved:', t.token);
        }, (reason) => {
          let toast = this.toastCtrl.create({
            message: reason,
            duration: 3000
          });
          toast.present();
        });
      }
      catch(e) {
        console.log((<Error>e).message);
      }
    });

    this.showLoading();


    auth.login(null).then(
      (isLoggedIn) => {
        this.hideLoading();
        if(isLoggedIn) {
          this.refreshProfile(null);
          Splashscreen.hide();
        } else {
          this.rootPage = WalkthroughPage;
          Splashscreen.hide();
        }
      }, (reason) => {
        this.hideLoading();
        console.log(reason);
        this.rootPage = WalkthroughPage;
      });

    events.subscribe('user:login', (event) => {
      console.log('Fired event: User login');
      this.isLoggedIn = true;
      this.rootPage = RecipePage;
    });
    events.subscribe('user:logout', (event) =>  {
      console.log('Fired Event: User logout');
      this.isLoggedIn = false;
      this.storage.clear();
      this.rootPage = WalkthroughPage;
    });

  }
  showLoading(msg: string = 'Espera...') {
    this.loader.setContent(msg);
    this.loader.present();
  }
  hideLoading() {
    this.loader.dismiss();
  }

  logout() {
    this.auth.logout().then((result) => {
      this.rootPage = LoginPage;
    });
  }
  
  ngAfterViewInit() {
    this.platform.ready().then(() => {

      // Convenience to route with a given nav
      Deeplinks.routeWithNavController(this.navChild, {

        '/receipt/:recipeId': RecipePage

      }).subscribe((match) => {
        console.log('Successfully routed', match);
      }, (nomatch) => {
        console.warn('Unmatched Route', nomatch);
      });
    });
  }
  profileEdit() {
    this.navChild.push(ProfilePage, {user: this.profilePreview.user});
  }
  refreshProfile(refresher) {
    if(!this.profilePreview) return;
    this.profilePreview.refresh().then(result => {
      if(refresher) refresher.complete();
    });
  }
}
