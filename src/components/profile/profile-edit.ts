import { Component } from '@angular/core';

/**
 * Generated class for the ProfileComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'profile-edit',
  templateUrl: 'profile-edit.html'
})
export class ProfileEditComponent {

  text: string;

  constructor() {
    console.log('Hello ProfileEditComponent Component');
    this.text = 'Hello World';
  }

}
