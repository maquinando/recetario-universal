import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ProfilePreviewComponent } from './profile-preview';
import { ProfileEditComponent } from './profile-edit';

@NgModule({
  declarations: [
    ProfilePreviewComponent,
    ProfileEditComponent
  ],
  imports: [
    IonicModule,
  ],
  exports: [
    ProfilePreviewComponent,
    ProfileEditComponent
  ]
})
export class ProfileComponentModule {}
