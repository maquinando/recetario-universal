// import { NavController } from 'ionic-angular';
import { Component } from '@angular/core';
import { Profile } from "../../providers/profile";


/**
 * Generated class for the ProfileComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'profile-preview',
  templateUrl: 'profile-preview.html'
})
export class ProfilePreviewComponent {

  public user: any = {};

  text: string;
  loading: boolean;

  constructor(private profileProvider: Profile/*, navCtrl: NavController*/) {
    console.log('Hello ProfilePreviewComponent Component');
    this.text = 'Hello World';
    
    // navCtrl.viewWillEnter.subscribe((result) => {
    //   console.log('View will enter.');
    //   console.log(result);
    // });
    this.refresh();
  }

  refresh() {
    this.loading = true;
    return new Promise((resolve) => {
      this.profileProvider.getProfile().then(user => {
        this.user = user; 
        this.loading = false;
        resolve(true);
      }, err => {
        console.error(err);
        resolve(false);
      });
    });
  }

}
