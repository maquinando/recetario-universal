import { Component, Input, EventEmitter, ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';

/**
 * Generated class for the Onboarding component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'onboarding',
  templateUrl: 'onboarding.html',
  outputs: ['onExit', 'onSlideChange']
})
export class Onboarding {

  @ViewChild(Slides) slideshow: Slides;
  @Input() slides: Onboardings;

  onExit:any = new EventEmitter();
  onSlideChange:any = new EventEmitter();

  text: string;

  constructor() {
  }
  continue() {
    this.onExit.next();
  }
  skip() {
    this.slideshow.slideTo(this.slideshow.length() - 1, 500);
  }
  slideChanged() {
      this.onSlideChange.next(this.slideshow.getActiveIndex());
  }



}



class OnboardingClass {

  title: string;
  subtitle: string;
  description: string;
  image: string;
  icon: string;

  constructor(title, subtitle, description, image, icon) {
    this.title = title;
    this.subtitle = subtitle;
    this.description = description;
    this.image = image;
    this.icon = icon;
  }
}

type Onboardings = Array<OnboardingClass>;
