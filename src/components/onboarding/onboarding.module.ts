import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Onboarding } from './onboarding';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    Onboarding,
  ],
  imports: [
    IonicPageModule.forChild(Onboarding),
    TranslateModule.forChild()
  ],
  exports: [
    Onboarding
  ]
})
export class OnboardingModule {}
