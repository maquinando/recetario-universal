

export class OnboardingClass {

  title: string;
  subtitle: string;
  description: string;
  image: string;
  icon: string;

  constructor(title, subtitle, description, image, icon) {
    this.title = title;
    this.subtitle = subtitle;
    this.description = description;
    this.image = image;
    this.icon = icon;
  }
}
