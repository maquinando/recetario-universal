import { LoadingController } from 'ionic-angular';
import { Component, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { GlobalValidator } from '../GlobalValidator';
import { Auth } from '../../providers/auth';

/*
  Generated class for the RegisterForm component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'register-form',
  templateUrl: 'register-form.html',
  outputs: ['onSuccess', 'onError'],
})
export class RegisterFormComponent {

  registerForm: FormGroup;

  onSuccess:any = new EventEmitter();
  onError:any = new EventEmitter();
  startTime:any;

  isLoading:boolean = false;

  constructor(formBuilder: FormBuilder, private auth: Auth, private loadingCtrl: LoadingController, private iab: InAppBrowser) {
    let startDate = new Date();
    startDate.setFullYear( startDate.getFullYear() - 20 );
    this.registerForm = formBuilder.group({
        email: ['', Validators.compose([Validators.required, Validators.minLength(5), GlobalValidator.mailFormat])],
        firstname: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
        lastname: ['', Validators.compose([Validators.required, Validators.minLength(5)])],
          password: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
          passwordConfirm: ['', GlobalValidator.passwordMatch],
        birthday: [startDate],
        gender: ['', Validators.compose([Validators.required])],
        country: ['', Validators.compose([Validators.required])],
        accept_tos: ['', Validators.required]
      });
  }

  register() {
    if(!this.registerForm.valid || this.isLoading) return;
    this.isLoading = true;
    console.log(this.registerForm.value);
    let loading = this.loadingCtrl.create({
      content: 'Espera un momento...'
    });
    loading.present();

    this.auth.register(this.registerForm.value).then((val) => {
      loading.dismiss();
      this.onSuccess.next(val);
      this.isLoading = false;
    }, (reason) => {
      loading.dismiss();
        this.onError.next(reason);
        this.isLoading = false;
    });
  }
  shouldEnableSubmit() {
    return (!this.registerForm.valid || this.isLoading==true);
  }
  tos() {
    let browser = this.iab.create(
      'https://www.hogaruniversal.com/terminos-y-condiciones',
      null,  
      {

      });

  }


}
