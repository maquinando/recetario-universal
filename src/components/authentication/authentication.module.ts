import { TranslateModule } from '@ngx-translate/core';
import { LoginFormComponent } from './login-form';
import { RegisterFormComponent } from './register-form';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';


@NgModule({
  declarations: [
    LoginFormComponent,
    RegisterFormComponent,
  ],
  imports: [
    IonicPageModule.forChild(LoginFormComponent),
    IonicPageModule.forChild(RegisterFormComponent),
    TranslateModule.forChild()
  ],
  exports: [
    LoginFormComponent,
    RegisterFormComponent,
  ]
})
export class RecetappAuthModule {}



