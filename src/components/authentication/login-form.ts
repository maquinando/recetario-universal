

import { Component, EventEmitter } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Auth } from '../../providers/auth';
import { GlobalValidator } from '../../components/GlobalValidator';

/*
  Generated class for the LoginForm component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
    */
  @Component({
    selector: 'login-form',
    templateUrl: 'login-form.html',
    outputs: ['onSuccess', 'onError'],
  })
  export class LoginFormComponent {

    loginForm : FormGroup;
    onSuccess:any = new EventEmitter();
    onError:any = new EventEmitter();
    isLoading:boolean = false;

    constructor(formBuilder: FormBuilder, private auth: Auth, public alertCtrl: AlertController, private loadingCtrl: LoadingController) {
      this.loginForm = formBuilder.group({
        email: ['', Validators.compose([Validators.required, Validators.minLength(5), GlobalValidator.mailFormat])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      });
    }

    login(){
      if( !this.loginForm.valid ) return;
      if(this.isLoading) return;
      this.isLoading = true;
      let loading = this.loadingCtrl.create({
        content: 'Espera un momento...'
      });
      loading.present();
      this.auth.login(this.loginForm.value).then((val) => {
        // window.location.reload();
        loading.dismiss();
        this.onSuccess.emit(val);
        this.isLoading = false;
      }, (reason) => {
        loading.dismiss();
        this.onError.emit(reason);
        this.isLoading = false;
      });
    }
    private recover(email) {
      if(this.isLoading) return;
      this.isLoading = true;
      let loading = this.loadingCtrl.create({
        content: 'Espera un momento...'
      });
      loading.present();
      this.auth.recover(email).then((val) => {
        loading.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Revisa tu correo electrónico',
          message: 'Hemos enviado un enlace de recuperación de contraseña a tu correo electrónico.'
        });
        alert.present();
        this.isLoading = false;
      }, (reason) => {
        loading.dismiss();
        this.onError.emit(reason);
        this.isLoading = false;
      });
      console.log(this.loginForm.value);
    }
    recoverPrompt() {
      let prompt = this.alertCtrl.create({
        title: '¿Olvidaste tu contraseña?',
        message: 'No te preocupes, nosotros te ayudaremos a recuperarla. Tan solo déjanos  tu correo  y te enviaremos de vuelta un enlace para recuperarla.',
        cssClass: 'password-recovery',
        inputs: [
        {
          name: 'email',
          placeholder: 'Correo Electrónico',
          type: 'email'
        },
        ],
        buttons: [

        {
          text: 'Enviar',
          handler: data => {
            console.log(data);
            this.recover(data.email.trim());
          }
        }
        ]
      });
      prompt.present();
    }
    shouldEnableSubmit() {
      return (!this.loginForm.valid || this.isLoading==true);
    }

  }
