
import { FormControl, FormGroup  } from '@angular/forms';



export class GlobalValidator{

  static mailFormat(control: FormControl): ValidationResult {

    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    if (control.value != "" && (control.value.length <= 5 || !EMAIL_REGEXP.test(control.value))) {
      return { "incorrectMailFormat": true };
    }
    return null;
  }
  static passwordMatch(control:FormControl): ValidationResult {
    // let pwd = this.registerForm.value.password
    let passwordControl = control.root.get('password');

    if(!passwordControl) {
      return { "incorrectPasswordMatch": true };
    }
    if (control.value == passwordControl.value) {
      return null;
    }
    return { "incorrectPasswordMatch": true };
  }
  // static  areEqual(group: FormGroup) {
  //   let valid = false;
  //   let prev = null;

  //   for (let name in group.controls) {
  //     console.log(name);
  //   }

  //   return null;
  // }



}

interface ValidationResult {
  [key: string]: boolean;
}

