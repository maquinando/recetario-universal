import { Component, Input, EventEmitter } from '@angular/core';
import { RecipeClass } from './recipe-class';

/**
 * Generated class for the RecipePreview component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'recipe-preview',
  templateUrl: 'recipe-preview.html',
  outputs: ['onSelect', 'onPrepare']
})
export class RecipePreview {

  @Input() recipe: any;

  text: string;

  onSelect:any = new EventEmitter();
  onPrepare:any = new EventEmitter();

  constructor() {
    // console.log(this.recipe);
  }

  view(event) {
    event.stopPropagation();
  	this.onSelect.next(this.recipe);
  }
  prepare(event) {
    event.stopPropagation();
  	this.onPrepare.next(this.recipe);
  }

}
