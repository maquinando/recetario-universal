import { Component, ViewChild } from '@angular/core';

import { NavController, NavParams, ViewController } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { TimerComponent } from './timer';


// import { InAppBrowser } from '@ionic-native/in-app-browser';


@Component({
  selector: 'page-prepara',
  templateUrl: 'preparation.html'
})
export class PreparationPage  {

  public testData: String;
  didPrepara:boolean = false;
  hasStarted:boolean = false;
  paused:boolean = true;
  currentStepCount:number = 0;
  currentStep:any = null;
  currentPreparation:any; 
  stepDuration: number;

  prepared = [];

  @ViewChild(TimerComponent) timer: TimerComponent;

  // private storeBrowser = this.iab.create('http://tiendahogaruniversal.com');

  recipe: any;
  
  constructor(public navCtrl: NavController, private navParams: NavParams , public storage: Storage, public viewCtrl: ViewController) {


    if(navParams.data.recipe !== undefined) {
      this.recipe = navParams.data.recipe;
      let countSteps = 0;
      this.recipe.preparations.forEach(preparation => {
        preparation.steps.foreach(step => {
          countSteps ++;
        })
      });
      if(countSteps > 0) {
        this.stepDuration = this.recipe.preparation_time / this.stepDuration;
      } else {
        this.stepDuration = this.recipe.preparation_time;
      }
    }

    storage.ready().then(()=> {

      storage.get('prepared').then((prepared) => {
        if(!prepared) prepared = [];
        prepared.push = this.recipe.id;
        storage.set('prepared', prepared);
      });
      storage.get('testData').then((val) => {
        this.testData = val;
      });
      
    });

    // setTimeout(() => {
    //   this.timer.startTimer();
    // }, 1000)


  }


  saveTestData(data) {
    this.testData = data;
    this.storage.set('testData', this.testData);
  }
  startStop() {
    if(!this.timer.timer.hasStarted) {
      this.timer.initTimer();
      this.timer.startTimer();
    }
    else if(this.timer.timer.runTimer) {
      this.timer.pauseTimer();
    } else if(this.timer.timer.hasFinished) {
      this.timer.initTimer();
    } else {
      this.timer.startTimer();
    }
  }

  goToStore() {
    // this.storeBrowser.show();
    // browser.executeScript(...);
    // browser.insertCSS(...);
    // browser.close();
  }
  nextStep() {
    if(!this.currentPreparation.steps  || !this.currentPreparation.steps.lenth) {
      this.currentPreparation = this.recipe.preparations.pop();
    }
    this.currentStep = this.currentPreparation.steps.pop();
    this.currentStepCount ++;
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
