export class StepClass {

}
export class PreparationClass {
    steps: Array<StepClass>;
}



export class RecipeClass {

  img: string;
  preview: string;
  preparationTime: number;
  slug: string;
  title: string;
  excerpt: string;
  cals: number;
  portions: number;
  content: string;
  id: number;

  constructor() {

  }
}




export type RecipesClass = Array<RecipeClass>;
export type PreparationsClass = Array<PreparationClass>;
