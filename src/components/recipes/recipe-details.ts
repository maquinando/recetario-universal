import { Storage } from '@ionic/storage';
import { PreparationPage } from './preparation';
import { Component, Input, EventEmitter, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ModalController, AlertController } from 'ionic-angular';
import {
  StackConfig,
  ThrowEvent,
  DragEvent,
  SwingStackComponent,
  SwingCardComponent} from 'angular2-swing';


/**
 * Generated class for the RecipePreview component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'recipe-details',
  templateUrl: 'recipe-details.html',
  outputs: ['onShowMore', 'onDismiss', 'onPrepare', 'onAddToFavorites']
})
export class RecipeDetails {
  cards: Array<any>;

  @Input() recipe: any;

  @ViewChild('myswing1') swingStack: SwingStackComponent;
  @ViewChildren('mycards1') swingCards: QueryList<SwingCardComponent>;

  text: string; 
  shownGroup: null;

  isFavorite:boolean = false;
  allFavorites:any = [];
  stackConfig: StackConfig;

  onPrepare:any = new EventEmitter();
  onDismiss:any = new EventEmitter();
  onShowMore:any = new EventEmitter();
  onAddToFavorites:any = new EventEmitter();

  constructor(public socialSharing: SocialSharing, public modalCtrl: ModalController, public storage: Storage, public alertCtrl: AlertController) {
    
    this.stackConfig = {
      throwOutConfidence: (offsetX, offsetY, element) => {
        return Math.min(Math.abs(offsetX) / (element.offsetWidth/2), 1);
      },
      transform: (element, x, y, r) => {
        this.onItemMove(element, x, y, r);
      },
      throwOutDistance: (d) => {
        return 800;
      }
    };

    console.log('Hello RecipeDetails Component');
    this.text = 'Hello World';
    storage.ready().then(() => {
      storage.get('favorites').then((result) => {
        if(!result) 
          result = [];
        this.allFavorites = result;
        if(this.allFavorites.includes(this.recipe.id)) {
          this.isFavorite = true;
        }
      }) 
    })
  }

  ngAfterViewInit() {
    this.setRecipe(this.recipe);
    // this is how you can manually hook up to the
    // events instead of providing the event method in the template
    this.swingStack.throwin.subscribe((event: DragEvent) => {
      event.target.style.background = '#ffffff';
    });
    this.swingStack.throwoutleft.subscribe(
      (event: ThrowEvent) => {
        console.log('Dismissed.');
        console.log(event);
        let removedCard = this.cards.pop();
        this.onDismiss.next(removedCard);
        // this.cards.push(this.recipe);
        return false;
      });
    this.swingStack.throwoutright.subscribe(
      (event: ThrowEvent) => {
        console.log('Add to favorites');
        console.log(event);

        // this.setFavorite();
        // let alert = this.alertCtrl.create({
        //   title: 'Agregado a Favoritos',
        //   subTitle: 'Receta agregada a favoritos exitosamente',
        //   buttons: ['Ok']
        // });
        // alert.present();
        
        this.prepare();

        let dismissed = this.cards.pop();
        setTimeout(() => {
          this.setRecipe(dismissed);
        }, 150);
        
        return false;
      });

  }


  share(service) {
    let message = this.recipe.title + '. Encuentra esta y otras espectaculares recetasn en @hogaruniversal. https://goo.gl/TrwiyX';
    let subject = 'Quisiera compartir contigo esta espectacular receta.';
    let url = 'http://recetapp.hogaruniversal.com/receta/' + this.recipe.slug;
    let image = this.recipe.img;
    let shareLink = '<a href="' + url + '">Ingresa aquí para ver la receta</a>';

    switch (service) {
      case "email":
        // code...
        this.socialSharing.shareViaEmail(message + '\n' + shareLink, subject, null, null, null, null).then(
          result => {this.shareSuccess(result);},
          reason => {this.shareError(reason);}
        );
        break;
      case "facebook":
        // code...
        this.socialSharing.shareViaFacebook(message, image, url).then(
          result => {this.shareSuccess(result);},
          reason => {this.shareError(reason);}
        );
        break;
      case "twitter":
        // code...
        this.socialSharing.shareViaTwitter(message, image, url).then(
          result => {this.shareSuccess(result);},
          reason => {this.shareError(reason);}
        );
        break;
      case "whatsapp":
        // code...
        this.socialSharing.shareViaWhatsApp(message, image, url).then(
          result => {this.shareSuccess(result);},
          reason => {this.shareError(reason);}
        );
        break;
      default:
        this.socialSharing.share(message, subject, null, url).then(
          result => {this.shareSuccess(result);},
          reason => {this.shareError(reason);}
        );
        break;
    }
  }
  private shareSuccess(result) {
    let alert = this.alertCtrl.create({
      title: 'Receta Compartida',
      subTitle: 'Receta compartida exitosamente. ' + result,
      buttons: ['Ok']
    });
    alert.present();
  }
  private shareError(reason) {
    let alert = this.alertCtrl.create({
      title: 'Error para compartir la receta',
      subTitle: reason,
      buttons: ['Ok']
    });
    alert.present();
  }

  setFavorite() {
    this.isFavorite = true;
    if(!this.allFavorites.includes(this.recipe.id)) {
      this.allFavorites.push(this.recipe.id);
      this.storage.set('favorites', this.allFavorites);
    }
  }
  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return (this.shownGroup === group) ? true : false;
  };
  prepare() {
    let prepareModal = this.modalCtrl.create(PreparationPage, { recipe: this.cards[0] });
      prepareModal.onDidDismiss((settings) => {
        console.log(settings);
      });
      prepareModal.present();
      this.onPrepare.next(this.recipe);
  }
  dismiss() {
    this.onDismiss.next(this.recipe);
  }

  setRecipe(recipe) {
    this.cards = [recipe];
  }









  // Called whenever we drag an element
onItemMove(element, x, y, r) {
  var color = '';
  var abs = Math.abs(x);
  let min = Math.trunc(Math.min(16*16 - abs, 16*16));
  let hexCode = this.decimalToHex(min, 2);
  
  if (x < 0) {
    color = '#FF' + hexCode + hexCode;
  } else {
    color = '#' + hexCode + 'FF' + hexCode;
  }
  
  element.style.background = color;
  element.style['transform'] = `translate3d(0, 0, 0) translate(${x}px, ${y}px) rotate(${r}deg)`;
}
 
 
// Add new cards to our array
addNewCards(count: number) {
  // this.http.get('https://randomuser.me/api/?results=' + count)
  // .map(data => data.json().results)
  // .subscribe(result => {
  //   for (let val of result) {
  //     this.cards.push(val);
  //   }
  // })
}
 
// http://stackoverflow.com/questions/57803/how-to-convert-decimal-to-hex-in-javascript
decimalToHex(d, padding) {
  var hex = Number(d).toString(16);
  padding = typeof (padding) === "undefined" || padding === null ? padding = 2 : padding;
  
  while (hex.length < padding) {
    hex = "0" + hex;
  }
  
  return hex;
}
}
