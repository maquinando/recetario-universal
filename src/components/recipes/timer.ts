import { Component, Input, EventEmitter } from '@angular/core';
import {ITimer} from './itimer';
import { DomSanitizer } from '@angular/platform-browser';



@Component({
    selector: 'timer',
    templateUrl: 'timer.html',
    outputs: ['onTimerEnd']
})
export class TimerComponent {

    @Input() timeInSeconds: number = 0;
    @Input() pause: boolean = false;
    @Input() shouldShowControls: boolean = true;
    @Input() step: number = 0;

    isInitiated:boolean = false;
    startTime:number;
    degree:number = 0;

    onTimerEnd:any = new EventEmitter();

    public timer: ITimer;

    constructor(private sanitizer: DomSanitizer) {
      // this.timeInSeconds = timeInSeconds;
        let basicStyle = 'linear-gradient(' + this.degree + 'deg, transparent 50%, #01386b 50%), linear-gradient(90deg, #01386b 50%, transparent 50%)';
        this.sanitizer.bypassSecurityTrustStyle('background: ' + basicStyle + ';');
    }
    stylish() {
        let basicStyle = 'linear-gradient(' + this.degree + 'deg, transparent 50%, #01386b 50%), linear-gradient(90deg, #01386b 50%, transparent 50%)';
        return this.sanitizer.bypassSecurityTrustStyle('background: -o-${basicStyle}; background: -moz-${basicStyle}; background: -webkit-${basicStyle};');
    }
    ngOnChanges(changes) {
        // console.log('Changed pause', changes.pause.currentValue, changes.paused.previousValue);
        // console.log('Changed time', changes.timeInSeconds.currentValue, changes.timeInSeconds.previousValue);
        if(this.isInitiated && changes.pause) {
            this.timer.runTimer = !changes.pause.currentValue;
        }
    }


    ngOnInit() {
        this.initTimer();
        this.timer.runTimer = !this.pause;
        this.isInitiated = true;
    }

    hasFinished() {
        this.onTimerEnd.next();
        return this.timer.hasFinished;
    }

    initTimer() {
        if(!this.timeInSeconds) { this.timeInSeconds = 0; }

        this.timer = <ITimer>{
            seconds: this.timeInSeconds,
            runTimer: false,
            hasStarted: false,
            hasFinished: false,
            secondsRemaining: this.timeInSeconds
        };
        if(this.step > 0 && this.timer.runTimer) {
            this.timer.displayTime = 'Paso ' + this.step;
        } else {
            this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.secondsRemaining);
        }
    }

    startTimer() {
        this.timer.hasStarted = true;
        this.timer.runTimer = true;
        this.timerTick();
    }

    pauseTimer() {
        if(this.timer)
            this.timer.runTimer = false;
    }

    resumeTimer() {
        this.startTimer();
    }

    timerTick() {
        setTimeout(() => {
            if (!this.timer.runTimer) { return; }
            this.timer.secondsRemaining--;
            this.timer.displayTime = this.getSecondsAsDigitalClock(this.timer.secondsRemaining);
            this.degree = 90 + (360 * (this.timer.seconds - this.timer.secondsRemaining) / this.timer.seconds);
            if (this.timer.secondsRemaining > 0) {
                this.timerTick();
            }
            else {
                this.timer.hasFinished = true;
            }
        }, 1000);
    }

    getSecondsAsDigitalClock(inputSeconds: number) {
        var sec_num = parseInt(inputSeconds.toString(), 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);
        var hoursString = '';
        var minutesString = '';
        var secondsString = '';
        hoursString = (hours < 10) ? "0" + hours : hours.toString();
        minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
        secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
        return (hours > 0? hoursString + ':' : '') + minutesString + ':' + secondsString;
    }

}
