import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecipePreview } from './recipe-preview';
import { RecipeDetails } from './recipe-details';
import { TimerComponent  } from './timer';
import { IonicImageLoader } from 'ionic-image-loader';
import { SwingModule } from 'angular2-swing';



@NgModule({
  declarations: [
    RecipePreview,
    RecipeDetails,
    TimerComponent,
  ],
  imports: [
    IonicPageModule.forChild(RecipePreview),
    IonicPageModule.forChild(RecipeDetails),
    SwingModule,
    IonicImageLoader
  ],
  exports: [
    RecipePreview,
    RecipeDetails,
    TimerComponent,
  ]
})
export class RecipePreviewModule {}



